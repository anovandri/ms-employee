import { Http } from '@dk/module-common';

enum ERROR_CODE {
  USER_AGE_NOT_VALID = 'USER_AGE_NOT_VALID',
  USER_NAME_EXISTED = 'USER_NAME_EXISTED',
  INCORRECT_EMAIL_FORMAT = 'INCORRECT_EMAIL_FORMAT',
  INCORRECT_FIELD = 'INCORRECT_FIELD',
  INVALID_REQUEST = 'INVALID_REQUEST',
  UNEXPECTED_ERROR = 'UNEXPECTED_ERROR', // do not use this when create AppError

  EMPLOYEE_EXISTED = 'EMPLOYEE_EXISTED',
  EMPLOYEE_DOES_NOT_EXIST = 'EMPLOYEE_DOES_NOT_EXISTS',
  EMPLOYEE_CAN_NOT_DELETE = 'EMPLOYEE_CAN_NOT_DELETE'
}

const JoiValidationErrors = {
  email: ERROR_CODE.INCORRECT_EMAIL_FORMAT
};

const ErrorList = {
  [ERROR_CODE.USER_AGE_NOT_VALID]: {
    statusCode: Http.StatusCode.BAD_REQUEST,
    message: 'sth'
  },
  [ERROR_CODE.USER_NAME_EXISTED]: {
    statusCode: Http.StatusCode.BAD_REQUEST,
    message: "User's name already existed"
  },
  [ERROR_CODE.INCORRECT_EMAIL_FORMAT]: {
    statusCode: Http.StatusCode.BAD_REQUEST,
    message: 'Incorrect email format'
  },
  [ERROR_CODE.INCORRECT_FIELD]: {
    statusCode: Http.StatusCode.BAD_REQUEST,
    message: 'Incorrect field value, data type or length'
  },
  [ERROR_CODE.INVALID_REQUEST]: {
    statusCode: Http.StatusCode.BAD_REQUEST,
    message: 'Invalid request'
  },
  [ERROR_CODE.UNEXPECTED_ERROR]: {
    statusCode: Http.StatusCode.INTERNAL_SERVER_ERROR,
    message: 'We caught unexpected error'
  },
  [ERROR_CODE.EMPLOYEE_EXISTED]: {
    statusCode: Http.StatusCode.BAD_REQUEST,
    message: 'Employee already existed'
  },
  [ERROR_CODE.EMPLOYEE_DOES_NOT_EXIST]: {
    statusCode: Http.StatusCode.BAD_REQUEST,
    message: "Employee doesn't exists"
  },
  [ERROR_CODE.EMPLOYEE_CAN_NOT_DELETE]: {
    statusCode: Http.StatusCode.INTERNAL_SERVER_ERROR,
    message: "Can't delete employee"
  }
};

export { ERROR_CODE, ErrorList, JoiValidationErrors };
