export default {
  getAll: jest.fn(),
  create: jest.fn(),
  update: jest.fn(),
  getById: jest.fn(),
  deleteById: jest.fn()
};
