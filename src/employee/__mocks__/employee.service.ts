export default {
  getEmployees: jest.fn(),
  createEmployee: jest.fn(),
  deleteEmployee: jest.fn(),
  updateEmployee: jest.fn()
};
