import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';

import { EmployeeModel } from '../employee.model';
import employeeRepository from '../employee.repository';

jest.mock('mongoose', () => {
  const mongoose = require.requireActual('mongoose');
  return new mongoose.Mongoose(); // new mongoose instance and connection for each test
});

describe('employee.repository', () => {
  let mongod: MongoMemoryServer;
  beforeAll(async () => {
    mongod = new MongoMemoryServer();
    const mongoDbUri = await mongod.getConnectionString();
    await mongoose.connect(mongoDbUri, { useNewUrlParser: true });
  });

  afterAll(async () => {
    mongoose.disconnect();
    mongod.stop();
  });

  describe('getAll', () => {
    it('should get all employee', async () => {
      await EmployeeModel.create(
        {
          id: '1112233',
          name: 'bagio',
          role: ['developer'],
          address: 'jakarta',
          gender: ['male']
        },
        {
          id: '444555666',
          name: 'sugeng',
          role: ['developer'],
          address: 'bogor',
          gender: ['male']
        }
      );

      const employees = await employeeRepository.getAll();
      expect(employees).toHaveLength(2);
    });
  });
});
