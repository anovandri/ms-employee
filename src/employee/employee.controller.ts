import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import employeeService from './employee.service';
import {
  EmployeeListResponseValidator,
  EmployeeResponseValidatorSchema,
  createEmployeeRequestValidator,
  EmployeeResponseValidator,
  deleteEmployeeRequestValidator
} from './employee.validator';
import { IEmployeeRequest, IRemoveEmployeeRequest } from './employee.interface';

const getEmployees: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: '/employees',
  options: {
    description: 'Get list of employees',
    notes: 'Get list of employees',
    tags: ['api', 'employee'],
    response: {
      schema: EmployeeListResponseValidator
    },
    handler: () => {
      return employeeService.getEmployees();
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employee retrieved'
          }
        }
      }
    }
  }
};

const createEmployee: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/employees',
  options: {
    description: 'Create new employee',
    notes: 'Create new employee',
    validate: {
      payload: createEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employee'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newEmployee = await employeeService.createEmployee(
        hapiRequest.payload
      );
      return hapiResponse.response(newEmployee).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Employee created.'
          }
        }
      }
    }
  }
};

const updateEmployee: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/employee/update',
  options: {
    description: 'Update employee',
    notes: 'Update employee',
    validate: {
      payload: EmployeeResponseValidatorSchema
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employee'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newEmployee = await employeeService.updateEmployee(
        hapiRequest.payload
      );
      return hapiResponse.response(newEmployee).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Employee updated.'
          }
        }
      }
    }
  }
};

const deleteEmployee: hapi.ServerRoute = {
  method: Http.Method.DELETE,
  path: '/employee/{employeeId}',
  options: {
    description: 'Delete employee by id',
    notes: 'Delete employee by id',
    validate: {
      params: deleteEmployeeRequestValidator
    },
    tags: ['api', 'employee'],
    handler: async (
      hapiRequest: IRemoveEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const { params } = hapiRequest;
      await employeeService.deleteEmployee(params.employeeId);
      return hapiResponse.response().code(Http.StatusCode.NO_CONTENT);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Employee deleted.'
          }
        }
      }
    }
  }
};

const employeeController: hapi.ServerRoute[] = [
  getEmployees,
  createEmployee,
  deleteEmployee,
  updateEmployee
];
export default employeeController;
