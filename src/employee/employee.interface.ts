import hapi from '@hapi/hapi';

import * as Joi from '@hapi/joi';

import 'joi-extract-type';

import {
  EmployeeResponseValidator,
  EmployeeResponseValidatorSchema
} from './employee.validator';

export type IEmployee = Joi.extractType<typeof EmployeeResponseValidatorSchema>;

export type IEmployeeResponse = Joi.extractType<
  typeof EmployeeResponseValidator
>;

export interface IEmployeeRequest extends hapi.Request {
  payload: IEmployee;
}

export interface IRemoveEmployeeRequest extends hapi.Request {
  params: {
    employeeId: string;
  };
}
