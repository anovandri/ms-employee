import mongoose, { Schema, Document, Model } from 'mongoose';
import { IEmployee } from './employee.interface';

export type EmployeeDocument = IEmployee & Document;

const EmployeeSchema: Schema = new Schema({
  id: {
    required: true,
    type: String
  },
  name: {
    required: true,
    type: String
  },
  role: {
    required: true,
    type: [String]
  },
  address: {
    required: true,
    type: String
  },
  gender: {
    type: [String]
  }
});

export const EmployeeModel: Model<EmployeeDocument> = mongoose.model(
  'Employee',
  EmployeeSchema
);
