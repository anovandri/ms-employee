import { EmployeeModel, EmployeeDocument } from './employee.model';
import { IEmployee, IEmployeeResponse } from './employee.interface';

const employeeDocumentToObject = (document: EmployeeDocument) =>
  document.toObject({ getters: true }) as IEmployeeResponse;

const employeeDocumentToObjects = (document: EmployeeDocument[]) =>
  document.map(employeeDocumentToObject);

const getAll = async () => {
  const documents = await EmployeeModel.find().exec();
  return employeeDocumentToObjects(documents);
};

const create = async (employee: IEmployee) => {
  const newEmployee = new EmployeeModel(employee);
  await newEmployee.save();
  return employeeDocumentToObject(newEmployee);
};

const update = async (employee: IEmployee) => {
  const newEmployee = new EmployeeModel(employee);
  await EmployeeModel.findOneAndUpdate({"id": employee.id}, employee);
  return employeeDocumentToObject(newEmployee);
}

const getById = async (id: string) => {
  const employee = await EmployeeModel.findOne({ id }).exec();
  return employee && employeeDocumentToObject(employee);
};

const deleteById = async (id: string) => {
  const result = await EmployeeModel.findOneAndDelete({ id }).exec();
  return result?.id
};

const employeeRepository = {
  getAll,
  create,
  update,
  getById,
  deleteById
};
export default employeeRepository;
