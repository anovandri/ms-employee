//import { ERROR_CODE } from '../common/errors';

import employeeRepository from './employee.repository';
import { IEmployee } from './employee.interface';
import { AppError } from '../errors/AppError';
import { ERROR_CODE } from '../common/errors';
//import { AppError } from '../errors/AppError';

const uuid = require('uuid');

const getEmployees = async () => {
  return employeeRepository.getAll();
};

const createEmployee = async (employee: IEmployee) => {
  employee.id = uuid.v1();
  return employeeRepository.create(employee);
};

const deleteEmployee = async (id: string) => {
  const existingEmployee = await employeeRepository.getById(id);
  if (!existingEmployee) {
    throw new AppError(ERROR_CODE.EMPLOYEE_DOES_NOT_EXIST);
  }
  const result = employeeRepository.deleteById(id);
  if (!result) {
    throw new AppError(ERROR_CODE.EMPLOYEE_CAN_NOT_DELETE);
  }
};

const updateEmployee = async (employee: IEmployee) => {
  const existingEmployee = await employeeRepository.getById(employee.id);
  if (!existingEmployee) {
    throw new AppError(ERROR_CODE.EMPLOYEE_DOES_NOT_EXIST);
  }
  return employeeRepository.update(employee);
};

const employeeService = {
  getEmployees,
  createEmployee,
  deleteEmployee,
  updateEmployee
};
export default employeeService;
