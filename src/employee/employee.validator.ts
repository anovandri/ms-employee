import * as Joi from '@hapi/joi';

import { MongooseBase } from '../common/validators';

const EmployeeResponseValidatorSchema = {
  id: Joi.string()
    .trim()
    .required(),
  name: Joi.string()
    .trim()
    .required(),
  role: Joi.array().items(Joi.string().valid('developer', 'qa', 'po', 'sm')),
  address: Joi.string()
    .trim()
    .required(),
  gender: Joi.array().items(Joi.string().valid('male', 'female'))
};

const EmployeeRequestValidatorSchema = {
  name: Joi.string()
    .trim()
    .required(),
  role: Joi.array().items(Joi.string().valid('developer', 'qa', 'po', 'sm')),
  address: Joi.string()
    .trim()
    .required(),
  gender: Joi.array().items(Joi.string().valid('male', 'female'))
};

const EmployeeResponseValidator = Joi.object({
  ...MongooseBase,
  ...EmployeeResponseValidatorSchema
})
  .required()
  .label('Response for Employee');

const EmployeeListResponseValidator = Joi.array()
  .items(EmployeeResponseValidator)
  .label('Response for list of employees');

const createEmployeeRequestValidator = Joi.object({
  ...EmployeeRequestValidatorSchema
}).label('Request create new employee');

const deleteEmployeeRequestValidator = Joi.object({
  employeeId: Joi.string()
    .trim()
    .required()
}).label('Delete employee based on id');

const updateEmployeeRequestValidator = Joi.object({
  ...EmployeeResponseValidatorSchema
}).label('Request update employee');

export {
  EmployeeResponseValidatorSchema,
  EmployeeRequestValidatorSchema,
  EmployeeResponseValidator,
  EmployeeListResponseValidator,
  createEmployeeRequestValidator,
  updateEmployeeRequestValidator,
  deleteEmployeeRequestValidator
};
