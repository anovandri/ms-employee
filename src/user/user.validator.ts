import * as Joi from '@hapi/joi';

import userConstant from './user.constant';

import { MongooseBase } from '../common/validators';

const UserValidator = {
  name: Joi.string()
    .trim()
    .required(),
  age: Joi.number()
    .min(userConstant.MIN_USER_AGE)
    .required()
};

const UserResponseValidator = Joi.object({
  ...MongooseBase,
  ...UserValidator
})
  .required()
  .label('Response - User');

const UserListResponseValidator = Joi.array()
  .items(UserResponseValidator)
  .label('Response - Users');

const createUserRequestValidator = Joi.object({ ...UserValidator }).label(
  'Request - new user'
);

export {
  UserResponseValidator,
  createUserRequestValidator,
  UserListResponseValidator,
  UserValidator
};
